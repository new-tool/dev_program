from src.duration import Duration


def build_duration_object(raw: dict):
    return Duration(raw['Hours'], raw['Minutes'], raw['Seconds'])