from src.duration_helper import build_duration_object
from src.helpers import get_name_detailed, resolve_participant, get_quantity_repeated_participants, get_list_repeated_participants, is_one, is_repeated

KEY_NAME = 'Name'
KEY_JOIN_TIME = 'Join time'
KEY_LEAVE_TIME = 'Leave time'
KEY_DURATION = 'Duration'
KEY_EMAIL = 'Email'
KEY_ROLE ='Role'

class ParticipantList:

    def __init__(self, raw_data, unique_key='Email', sort_key='Join Time'):
        self.data = raw_data
        self.unique_key = unique_key
        self.sort_key = sort_key

    def sort(self, key='Join time', ascending=True):
        """It returns a new list instance sorted by specified key"""
        if not(key in ['Name', 'Join time', 'Leave time', 'Duration' , 'Email', 'Role']):
            return []
        
        sorted_data = self.data[:]
        sorted_data.sort(key = lambda k: k.get(key), reverse= not(ascending))        
        return sorted_data

    def  get_one_time_participants(self, key):
        list_repeated_participants = get_list_repeated_participants(self.data, key)
        list_one_time_participants = list(filter(is_one, list_repeated_participants))
        return list_one_time_participants
    
    def get_repeated_participants(self, key):
        list_repeated_participants = get_list_repeated_participants(self.data, key)
        list_one_time_participants = list(filter(is_repeated, list_repeated_participants))
        return list_one_time_participants

    @property
    def participants(self):
        if self.data == []:
            return []
        
        if self.data is None:
            return 
        
        repeated_participants = self.get_repeated_participants(self.unique_key)
        normalized_repeated_participants = [resolve_participant(repeated) for repeated in repeated_participants]
        one_time_participants = self.get_one_time_participants(self.unique_key)
        normalized_one_time_participants = [resolve_participant(participant) for participant in one_time_participants]
        list_normalized_participants = normalized_one_time_participants + normalized_repeated_participants
        list_normalized_participants.sort(key = lambda k: (k.get('First join')))      
        return list_normalized_participants