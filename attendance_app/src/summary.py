from src.duration import Duration


class Summary:
    
    def __init__(self, raw):
        self.title = raw.get('Title')
        self.id = raw.get('Id')
        self.attended_participants = raw.get('Attended participants') or raw.get('Total Number of Participants') or 0
        self.start_time = raw.get('Start Time')
        self.end_time = raw.get('End Time')
        self.duration = Duration(raw.get('Duration').get('Hours'), raw.get('Duration').get('Minutes'), raw.get('Duration').get('Seconds'))

    def __str__(self) -> str:
        return f"{{'Title': {self.title}, 'Id': {self.id}, 'Attended participants':{self.attended_participants}, 'Start Time': {self.start_time}, 'End Time': {self.end_time}, 'Duration': {str(self.duration)}}}"
        