from os import listdir, mkdir
from os.path import isfile, join
import csv
from datetime import datetime
from datetime import timedelta

STAGE_DATA = 'attendance_app/stage_data'
KEY_NAME = 'Name'
KEY_NAME_V1 = 'Full Name'
KEY_JOIN_TIME = 'Join time'
KEY_JOIN_TIME_V1 = 'Join Time'
KEY_LEAVE_TIME = 'Leave time'
KEY_LEAVE_TIME_V1 = 'Leave Time'
KEY_DURATION = 'Duration'
KEY_EMAIL = 'Email'
KEY_ROLE ='Role'
KEY_PARTICIPANT = 'Participant ID (UPN)'


def discover_data_files(path='../data', pattern='csv'):
    return [join(path, f) for f in listdir(path) if isfile(join(path, f)) and f.endswith('csv')]


def read_file(files):
    readers = []
    for  f in files:
        csv_file = open(f, encoding='UTF-16')
        readers.append((f, csv.reader(csv_file, delimiter='\t')))
    return readers


def create_dir(dir_name, path = STAGE_DATA):
    try:
        dir_folder = f'{path}/{dir_name}'
        mkdir(dir_folder)
        return dir_folder
    except OSError as e:
        return f'Error in creation /{dir_name}, {e} '


def update_keys(raw: dict, map_table:dict):
    if raw is None or raw == {} or raw == []:
        return {}

    dict_ = {}
    for key, value in raw.items():
        if key not in map_table.keys():
            map_table.update({key: key})
        dict_.update({map_table.get(key): value})
    return dict_


def get_name_and_sur_name(email):
        full_name = email.split("@")[0]
        name = full_name.split('.')[0]
        first_surname = full_name.split('.')[1]
        return (name, first_surname)


def get_name_detailed(raw):
    name, first_surname = get_name_and_sur_name(raw['Email'])
    full_name = raw['Full Name'].split(' ')
    print(full_name)
    middle_name = None
    second_surname = None
    if len(full_name) > 2 :
        if first_surname == full_name[-1]:
            middle_name = full_name[-2]
        if first_surname == full_name[-2]:
            second_surname = full_name[-1]
        if first_surname != full_name[1]:
            middle_name = full_name[1]
    return (name, middle_name, first_surname, second_surname)


def get_date(str_date: str, format_date = "%m/%d/%y, %I:%M:%S %p"):
    return datetime.strptime(str_date, format_date)


def get_duration(start_time: datetime, end_time: datetime):
    duration = end_time - start_time
    duration = str(duration)
    return tuple(map(int, duration.split(":")))


def resolve_participant(sample_repeated_in_out: list):
    if not sample_repeated_in_out:
        return
    join_time = get_min_join_time(sample_repeated_in_out)
    leave_time = get_max_leave_time(sample_repeated_in_out)
    total_duration = get_total_duration_v2(sample_repeated_in_out)
    name = sample_repeated_in_out[0].get(KEY_NAME) or sample_repeated_in_out[0].get(KEY_NAME_V1)
    email = sample_repeated_in_out[0].get(KEY_EMAIL) or sample_repeated_in_out[0].get(KEY_PARTICIPANT)
    role = sample_repeated_in_out[0].get(KEY_ROLE) 
    return {
        'Name': name,
        'First join': join_time,
        'Last leave': leave_time,	
        'In-meeting duration': total_duration,
        'Email': email,
        'Participant ID (UPN)': email,
        'Role': role
    }
    

def get_quantity_repeated_participants(data_sorted: list, unique_key='Email') -> list:
    counts = []
    cont = 1
    if len(data_sorted) <= 1:
        return [0]
    for i in range(1, len(data_sorted)):
        if data_sorted[i-1].get(unique_key) == data_sorted[i].get(unique_key):
            cont += 1
        else:
            counts.append(cont)
            cont = 1
    counts.append(cont)
    return counts    


def get_list_repeated_participants(data: list, unique_key = 'Email') -> list:
        data_sorted = data[:]
        data_sorted.sort(key = lambda k: k.get(unique_key))
        list_ocurrences = get_quantity_repeated_participants(data_sorted, unique_key)
        output = []
        index = 0
        for ocurrence in list_ocurrences:
            output.append(data_sorted[index:index+ocurrence])
            index += ocurrence
        return output
    
def is_one(data: list) -> bool:
    return True if len(data) == 1 else False


def is_repeated(data: list) -> bool:
    return True if len(data) > 1 else False    


def get_min_join_time(sample_repeated_in_out: list):
    if not sample_repeated_in_out:
        return ''
    sample_repeated_in_out.sort(key = lambda k: (k.get(KEY_JOIN_TIME) or k.get(KEY_JOIN_TIME_V1)))
    return sample_repeated_in_out[0].get(KEY_JOIN_TIME) or sample_repeated_in_out[0].get(KEY_JOIN_TIME_V1)


def get_max_leave_time(sample_repeated_in_out: list):
    if not sample_repeated_in_out:
        return ''
    sample_repeated_in_out.sort(key = lambda k: (k.get(KEY_LEAVE_TIME)or k.get(KEY_LEAVE_TIME_V1)))
    return sample_repeated_in_out[-1].get(KEY_LEAVE_TIME) or sample_repeated_in_out[-1].get(KEY_LEAVE_TIME_V1)   


def get_time_from_seconds(total_seconds: int) -> str:
    time = str(timedelta(seconds=total_seconds))
    hour, minutes, seconds = time.split(':')
    str_hour = str(int(hour))+'h ' if int(hour) != 0 else ''
    str_min = str(int(minutes))+'m ' if int(minutes) != 0 else ''
    str_sec= str(int(seconds))+'s'
    return f'{str_hour}{str_min}{str_sec}'


def get_total_seconds_from_list(time: list) -> int:
    hours, min, sec = 0, 0, 0
    for elem in time:
        if elem.endswith('h'):
            hours = int(elem[:-1])
        if elem.endswith('m'):
            min = int(elem[:-1])
        if elem.endswith('s'):
            sec = int(elem[:-1])    
    return (hours*3600) + (min * 60) + sec


def get_total_duration_v2(sample_repeated_in_out: list):
    total_seconds = 0
    for raw in sample_repeated_in_out:
        time_list = raw['Duration'].split(' ')
        total_seconds += get_total_seconds_from_list(time_list)
    str_duration = get_time_from_seconds(total_seconds)
    return str_duration
