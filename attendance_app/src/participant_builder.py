from src.participant import ParticipantList
from src.helpers import update_keys, get_date, get_duration
import re
from datetime import timedelta


KEY_NAME = 'Full Name'
KEY_NAME_V2 = 'Name'
KEY_JOIN_TIME = 'Join Time'
KEY_JOIN_TIME_V1 = 'First join'
KEY_JOIN_TIME_V2 = 'Join time'
KEY_LEAVE_TIME = 'Leave Time'
KEY_LEAVE_TIME_V1 = 'Last leave'
KEY_LEAVE_TIME_V2 = 'Leave time'
KEY_DURATION = 'Duration'
KEY_INMEETING = 'In-meeting duration'
KEY_EMAIL = 'Email'
KEY_ROLE = 'Role'
KEY_PARTICIPANT = 'Participant ID (UPN)'


def get_duration_str(time_duration: str):
    hms_letters = re.findall(r'[hms]', time_duration)
    hms_numbers = re.findall(r'\d*[^hms ]',
    time_duration)
    duration = dict(zip(hms_letters, hms_numbers))
    if 'h' not in duration.keys():
        duration['h'] = '0'
    if 'm' not in duration.keys():
        duration['m'] = '0'
    if 's' not in duration.keys():
        duration['s'] = '0'
    return {
        'Hours': int(duration['h']),
        'Minutes': int(duration['m']),
        'Seconds': int(duration['s']),	
    }


def normalize_in_meeting(raw: dict) -> dict:
    name = raw.get(KEY_NAME) or raw.get(KEY_NAME_V2)
    join_time = raw.get(KEY_JOIN_TIME) or raw.get(KEY_JOIN_TIME_V2)
    leave_time = raw.get(KEY_LEAVE_TIME_V2) or raw.get(KEY_LEAVE_TIME)
    duration = raw.get(KEY_DURATION)
    email = raw.get(KEY_EMAIL)
    role = raw.get(KEY_ROLE)
    normalized_in_meeting = {
        'Name': name,
        'Join time': join_time,
        'Leave time': leave_time,
        'Duration': duration,
        'Email': email,
        'Role': role
    }
    return normalized_in_meeting


def normalize_participant(raw: dict):
    name = raw.get(KEY_NAME) or raw.get(KEY_NAME_V2)
    join_time = raw.get(KEY_JOIN_TIME) or raw.get(KEY_JOIN_TIME_V2)
    leave_time = raw.get(KEY_LEAVE_TIME) or raw.get(KEY_LEAVE_TIME_V2)
    in_meeting_duration = raw.get(KEY_DURATION)
    email = raw.get(KEY_EMAIL)
    role = raw.get(KEY_ROLE)
    normalized_participant = {
        'Name': name, 
        'First join': join_time,
        'Last leave': leave_time,
        'In-meeting duration': in_meeting_duration,
        'Email': email,
        'Participant ID (UPN)': email,
        'Role': role
    }
    return normalized_participant


def normalize_in_meetings(in_meetings: list[dict]):
    output = []
    for in_meeting in in_meetings:
        in_meeting_normalized = normalize_in_meeting(in_meeting)
        verify_not_None_values(in_meeting_normalized)
        output.append(in_meeting_normalized)
        
    return output


def normalize_participants(participant_in_meeting: list[dict]):
    list_normalized = []
    if len(participant_in_meeting) == 1:
        only_one_participant = normalize_participant (participant_in_meeting[0])
        list_normalized.append(only_one_participant)
        return list_normalized
        
    participant_in_meeting = ParticipantList(participant_in_meeting)
    normalized_participants = participant_in_meeting.participants
    for participant in normalized_participants:
        list_normalized.append(participant)
        
    return list_normalized    


def verify_not_None_values(dict_data: dict):
    for value in dict_data.values():
        if value == None:
            raise Exception('Some value is giving None - error during normalize in meetings activities.')
