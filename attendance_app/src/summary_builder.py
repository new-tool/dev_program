from src.summary import Summary
from src.duration import Duration
from src.helpers import update_keys, get_date, get_duration

FORMAT_DATE = "%m/%d/%Y, %I:%M:%S %p";
summary_map_table ={
    'Title': 'Title',
    'Id': 'Id',
    'Attended participants': 'Attended participants',
    'Start Time': 'Start Time',
    'End Time': 'End Time',
    'Duration': 'Duration',
    'Meeting title': 'Title',
    'Meeting Title': 'Title',
    'Start time': 'Start Time',
    'Meeting Start Time': 'Start Time',
    'End time': 'End Time',
    'Meeting End Time': 'End Time',
    'Meeting duration': 'Duration',
    'Meeting Id': 'Id',
    'Total Number of Participants': 'Attended participants',
}


def normalize_summary(raw_data: dict):
    new_raw = update_keys(raw_data, summary_map_table)
    if 'Id' not in new_raw.keys():
        new_raw.update({'Id': new_raw.get('Title')})
    year = new_raw.get('Start Time').split(',')[0].split('/')[-1]
    if len(year) == 2:
        start_time = get_date(new_raw.get('Start Time'))
        end_time = get_date(new_raw.get('End Time'))
        hours, minutes, seconds = get_duration(start_time, end_time)
    if len(year) == 4:
        start_time = get_date(new_raw.get('Start Time'), FORMAT_DATE)
        end_time = get_date(new_raw.get('End Time'), FORMAT_DATE)
        hours, minutes, seconds = get_duration(start_time, end_time)
    return {
            'Title': new_raw.get('Title'),
            'Id': new_raw.get('Id'),
            'Attended participants': int(new_raw.get('Attended participants')),
            'Start Time': new_raw.get('Start Time'),
            'End Time': new_raw.get('End Time'),
            'Duration': {
                'Hours': hours,
                'Minutes': minutes,
                'Seconds': seconds,
            }
        }


def build_summary_object(raw_data: dict):
   return Summary(normalize_summary(raw_data))